#!/usr/bin/env python
import esptool
import serial
import time
import RPi.GPIO as GPIO
import sys


class Serial_GPIO:
    def __init__(self):
        # self.command = command
        # self._serial = self.setup_serial()
        # self.send(self.command)
        self.conn()
        pass
        

    def conn(self, port_name = "/dev/ttyS0"):
        try:
            self._serial = serial.Serial(
                port=port_name,
                baudrate=9600,
                bytesize=serial.EIGHTBITS,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_TWO,
                timeout=1,
                rtscts=0
                )
            self._serial.reset_input_buffer()
            self._serial.reset_output_buffer()
            time.sleep(1)
            return self._serial
        except Exception as e:
            return str(e)


    
    def send(self, command):
        try:
            answer = 'NULL'
            # Checks if Serial communication is still open  
            if self._serial.isOpen():
                # Encode all commands to byte
                serial_in = command.encode()

                # Send command
                #ser.write(serial_in)
                self._serial.write(serial_in)

                # Read the output until find tag "EOL**" and decode it
                answer = self._serial.read_until(b"*EOL**").decode()

                # Remove all \r of output
                answer = answer.replace("\r", "")

                #Close Serial
                #self.close_serial()

            return answer

        except Exception as e:
            return str(e)

    def close_serial(self):
        try:
            self._serial.close()
        except Exception as e:
            return str(e)

    
if __name__ == "__main__":
    print(' ---------------------------------------------------------')
    print(' -- If you need to know the commands, send "h" to help! --')
    print(' ---------------------------------------------------------')

    try:
        # Create Serial Object:
        arduino_obj = Serial_GPIO()

        while 1:
            x = input("Insert command >> ")
            
            # Send Command:
            zau = arduino_obj.send(x)
            print(zau)

        # Close connection:
        arduino_obj.close_serial()
        
    except KeyboardInterrupt:    
        # Close connection:
        arduino_obj.close_serial()
    except Exception as e:
        print(str(e))


    # # Create Serial Object:
    # try:
    #     ser = setup_serial("/dev/ttyS0")
    # except Exception as e:
    #     print("Error: " + str(e))

    # try:
    #     while 1:
    #         x = input("Insert command >> ")
    #         string = x.encode()
    #         # print("Enviado pelo Script:" + str(string))
    #         ser.write(string)

    #         response = ser.read_until('*EOL**')
    #         if len(response) > 0:
    #             # print("Recebido pelo Script: " + response.decode())
    #             print(response.decode())
    #         else:
    #             print("Null")
    # except KeyboardInterrupt:    
    #     ser.close()

