String FW = "Probe V2 Arduino Micro Controller";
String last_update = "ML 13/07/2020 12:55";

///////////////
//Variáveis////
///////////////
#define INA1 A1               // Porta INA1;
#define INA2 A2               // Porta INA2;
#define rele1 11              // Porta Rele1
#define rele2 12              // Porta Rele2


const float RS = 0.05;        // Shunt resistor value (in ohms) on INA169
const float RL = 10;          // Shunt resistor value (in ohms) on INA169
const int VOLTAGE_REF = 5;    // Reference voltage for analog read
int var;                      // variavel do que a porta serie do arduino lê;
int sensorValue;              // valor absoluto lido pela ADC;
float analog_voltage;         // tensão lida na ADC;
float current;                // corrente lida na ADC;


void setup() {
  Serial1.begin(9600);        //Configura o BaudRate da porta serie
  pinMode(INA1, INPUT);       //Configura INA1
  pinMode(INA2, INPUT);       //Configura INA2
  pinMode(rele1, OUTPUT);     //Configura Rele1
  pinMode(rele2, OUTPUT);     //Configura Rele1

  delay(500);
}

void Rele_Controller(int rele, String do_it, String position_)
{
  //String posicao = get_position(rele);
  if (do_it == "on")
  {
    digitalWrite(rele, HIGH);
    delay(100);

    Serial1.print("Rele");
    Serial1.print(position_);
    Serial1.print(" ON");


    //    Serial1.print("*");
    //    Serial1.print(posicao);
    //    Serial1.print("*");
    //    Serial1.print("BOL");
    //    Serial1.print("*");
    //    Serial1.print(posicao);
    //    Serial1.print("*");
    //
    //    Serial1.print("Rele ");
    //    Serial1.print(posicao);
    //    Serial1.print(" ON");
    //
    //    Serial1.print("*");
    //    Serial1.print(posicao);
    //    Serial1.print("*");
    //    Serial1.print("EOL");
    //    Serial1.print("*");
    //    Serial1.print(posicao);
    //    Serial1.println("*");

  }
  else if (do_it == "off")
  {
    digitalWrite(rele, LOW);

    delay(100);

    Serial1.print("Rele");
    Serial1.print(position_);
    Serial1.print(" OFF");

    //    Serial1.print("*");
    //    Serial1.print(posicao);
    //    Serial1.print("*");
    //    Serial1.print("BOL");
    //    Serial1.print("*");
    //    Serial1.print(posicao);
    //    Serial1.print("*");
    //
    //    Serial1.print("Rele ");
    //    Serial1.print(posicao);
    //    Serial1.print(" OFF");
    //
    //    Serial.print("*");
    //    Serial.print(posicao);
    //    Serial.print("*");
    //    Serial.print("EOL");
    //    Serial.print("*");
    //    Serial.print(posicao);
    //    Serial.println("*");

  }
  else
  {
    digitalWrite(rele, LOW);
    delay(100);

    Serial1.print("Digital");
    Serial1.print(position_);
    Serial1.print(" OFF");

    //    Serial1.print("*");
    //    Serial1.print(posicao);
    //    Serial1.print("*");
    //    Serial1.print("BOL");
    //    Serial1.print("*");
    //    Serial1.print(posicao);
    //    Serial1.print("*");
    //
    //    Serial1.print("Rele ");
    //    Serial1.print(posicao);
    //    Serial1.print(" OFF");
    //
    //    Serial1.print("*");
    //    Serial1.print(posicao);
    //    Serial1.print("*");
    //    Serial1.print("EOL");
    //    Serial1.print("*");
    //    Serial1.print(posicao);
    //    Serial1.println("*");
  }

  Serial1.println("*EOL**");

}

void Reset_all_Reles()
{
  Rele_Controller(rele1, "off", "1");
  Rele_Controller(rele2, "off", "2");
}

void Current_Meas(int amp)
{
  //  Serial1.print("AMP: ");
  //  Serial1.println(amp);

  int i = 0;
  int cycles = 10;
  float current_summation = 0;
  float average_value = 0;
  sensorValue = 0;
  analog_voltage = 0;
  current = 0;

  for (i; i < cycles; i++)
  {
    sensorValue = analogRead(amp);

    //    Serial1.print("Valor absoluto: ");
    //    Serial1.print(sensorValue);
    //    Serial1.println();

    analog_voltage = (sensorValue * VOLTAGE_REF) / 1023.0;

    //    Serial1.print("Voltagem: ");
    //    Serial1.print(analog_voltage);
    //    Serial1.print(" V");
    //    Serial1.println();

    current = analog_voltage / (RL * RS) * 1000;

    //    Serial1.print(current, 0);
    //    Serial1.println(" mA");

    current_summation += current;

    delay(50);
  }

  //  Serial1.print(current_summation);
  //  Serial1.println(" mA");
  //  Serial1.print(i);
  //  Serial1.println(" ciclos");

  average_value = (current_summation / i);

  Serial1.print(average_value, 0);
  Serial1.println("*EOL**");

}

void serie()
{
  var = Serial1.read();
  if (true) {
    switch (var) {

      case 'q':
        {
          Current_Meas(INA1);
        }
        break;

      case 'w':
        {
          Current_Meas(INA2);
        }
        break;

      case 'a':
        {
          Rele_Controller(rele1, "on", "1");
        }
        break;

      case 's':
        {
          Rele_Controller(rele1, "off", "1");
        }
        break;

      case 'd':
        {
          Rele_Controller(rele2, "on", "2");
        }
        break;

      case 'f':
        {
          Rele_Controller(rele2, "off", "2");
        }
        break;

      case 'z':
        {
          Reset_all_Reles();          //Reset All Reles
        }
        break;

      case 'v':
        {
          Serial1.println();
          Serial1.println(FW);
          Serial1.println(last_update);
          Serial1.println("*EOL**");
        }
        break;
        case 'h':
        {
          Serial1.println("#######################");
          Serial1.println("## Serial Commands: ##");
          Serial1.println("q -> INA1");
          Serial1.println("w -> INA2");
          Serial1.println("a -> Rele 1 ON");
          Serial1.println("s -> Rele 1 OFF");
          Serial1.println("d -> Rele 2 ON");
          Serial1.println("f -> Rele 2 OFF");
          Serial1.println("z -> Reset All Reles");
          Serial1.println("v -> version");
          Serial1.println("#######################");
          Serial1.println("*EOL**");
        }
        break;

      default:
        //não faz nada
        ;
    }
    //Reset as variaveis:
    var = NULL; //limpar porta serie e parar o loop
  }
}




void loop()
{
  serie();
}
